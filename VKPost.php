<?php

namespace SocialAutomation\VK;

class VKPost extends VKAttachment {
    /*
      suggests — предложенные записи на стене сообщества (доступно только при вызове с передачей access_token);
      postponed — отложенные записи (доступно только при вызове с передачей access_token);
      owner — записи на стене от ее владельца;
      others — записи на стене не от ее владельца;
      all — все записи на стене (owner + others). */

    const FILTER_SUGGESTS = 'suggests';
    const FILTER_POSTPONED = 'postponed';
    const FILTER_OWNER = 'owner';
    const FILTER_OTHERS = 'others';
    const FILTER_ALL = 'all';

    private $author;
    private $date;
    private $post_type;
    private $text;
    private $source;
    private $copy_history = Array();
    private $photos = Array();
    private $videos = Array();
    private $audios = Array();
    private $documents = Array();
    private $link;
    private $likes;
    private $reposts;
    private $can_publish;

    public function __construct($post) {

        parent::__construct($post->owner_id, $post->id);

        VKDebug::debug_construct($this, $this->url_html(), substr($post->text, 0, 100), $post->source);

        //$this->author = new VKOwner((int) $post->from_id);

        try{
            $this->author = VKOwnerFactory::from_id($post->from_id);
        } catch (\Exception $e){
            VKDebug::debug($e->getMessage());
        }

        //echo "<br>".var_dump($this->author)."<br>";

        $this->date = (int) $post->date;
        $this->post_type = $post->post_type;
        $this->text = $post->text;
        $this->likes = (int) $post->likes->count;
        $this->reposts = (int) $post->reposts->count;

        $this->can_publish = ($post->likes->can_publish != '0');

        $this->source = $post->source;
        
        if ($post->copy_history){
            foreach ($post->copy_history as $post_data){
                array_push($this->copy_history, new VKPost($post_data));
            }
        }


        if ($post->attachments) {
            foreach ($post->attachments as $attachment) {
                switch ($attachment->type) {
                    case "audio": array_push($this->audios, new VKAudio($attachment->audio));
                        break;
                    case "video": array_push($this->videos, new VKVideo($attachment->video));
                        break;
                    case "photo": array_push($this->photos, new VKPhoto($attachment->photo));
                        break;
                    case "doc": array_push($this->documents, new VKDocument($attachment->doc));
                        break;
                    case "link": $this->link = new VKLink($attachment->link);
                }
            }
        }
    }
    
    /*public static function from_attachment(VKAttachment $attachment){
        
        $post_data[owner_id] = $attachment->owner_id();
        $post_data[id] = $attachment->id();
        $post = new VKPhoto($post_data);
        return $post;
        
    }*/

    public function get_attachments() {
        return array_merge($this->photos, $this->videos, $this->audios, $this->documents);
    }

    public function attachments_size() {
        return sizeof($this->get_attachments());
    }

    public function likes() {
        return $this->likes;
    }
    
     public function reposts() {
        return $this->reposts;
    }

    public function photos() {
        return $this->photos;
    }

    public function photos_size() {
        return sizeof($this->photos);
    }

    public function videos() {
        return $this->videos;
    }

    public function videos_size() {
        return sizeof($this->videos);
    }

    public function audios() {
        return $this->audios;
    }

    public function audios_size() {
        return sizeof($this->audios);
    }

    public function documents() {
        return $this->documents;
    }

    public function documents_size() {
        return sizeof($this->documents);
    }

    public function get_string() {
        return $this->type() . parent::get_string();
    }

    public function date() {
        return $this->date;
    }

    public function text() {
        return $this->text;
    }

    public function has_text() {
        return $this->text != "";
    }

    public function post_type() {
        return $this->post_type;
    }

    public function from_id() {
        return $this->author->id();
    }

    public function author() {
        return $this->author;
    }
    
    public function copy_history(){
        return $this->copy_history;
    }

    public function is_repost() {
        return sizeof($this->copy_history) > 0;//$this->copy_history != NULL;
    }

    public function is_profile_photo() {
        return $this->source->data == 'profile_photo';
    }

    public function has_link() {
        return $this->link != NULL;
    }
    
    public function has_references(){
        return preg_match('/\[.+\]/', $this->text) == true;
    }

    public function can_publish() {
        return $this->can_publish;
    }

    public function link() {
        return $this->link;
    }
    
    
    public function type() {
        return VKAttachment::POST_WALL;
    }

    public function type_like() {
        return VKAttachment::POST;
    }
}
