## SocialAutomation\VK ##
 * A PHP library to work with vk.com API v5.45

### Features: ###
 * Post 
 * Like / unlike
 * Fetch posts and users
 * Upload attachments
 * Search for popular music
 * Store small data locally or in vk storage 
 * MySQL database storage
 * PSR-4 Compliance

### TODO: ###
 * PSR
 * exception codes table
 * more features...