<?php

namespace SocialAutomation\VK;

//generate an attachment from the string
//for example with input string wall321_123
//the object VKPost(owner_id=321,id=123) will be returned
abstract class VKAttachmentFactory {

    public static function from_string($string) {
        if (!preg_match("/([a-z]+)([-\d]+)_(\d+)/", $string, $matches)) {
            throw new \Exception("Unable to create an attachment from string");
        }

        $type = $matches[1];
        $attachment = new VKAttachment($matches[2], $matches[3]);
         
        switch ($type) {
            case VKAttachment::POST_WALL:
                return new VKPost($attachment->data());
            case VKAttachment::PHOTO:
                return new VKPhoto($attachment->data());
            case VKAttachment::VIDEO:
                return new VKVideo($attachment->data());
            case VKAttachment::AUDIO:
                return new VKAudio($attachment->data());
            case VKAttachment::DOCUMENT:
                return new VKDocument($attachment->data());
        }

    }

}
