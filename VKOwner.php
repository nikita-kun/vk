<?php

namespace SocialAutomation\VK;

//base class
//owner is determined by signed int id
class VKOwner {

    private $id;

    function __construct($id) {
        $this->id = (int) $id;
    }

    public function id() {
        return $this->id;
    }

    public function date_id() {
        return date("Y-m-d") . " " . $this->id;
    }

}
