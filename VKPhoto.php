<?php

namespace SocialAutomation\VK;

class VKPhoto extends VKAttachment {

    private $album_id;
    private $width;
    private $height;
    private $text;
    private $date;

    public function __construct($photo) {

        parent::__construct($photo->owner_id, $photo->id);

        $this->album_id = (int) $photo->album_id;
        $this->width = (int) $photo->width;
        $this->height = (int) $photo->height;
        $this->text = $photo->text;
        $this->date = (int) $photo->date;

        VKDebug::debug_construct($this, $this->get_string(), "$this->width * $this->height");
    }
    
    /*public static function from_attachment(VKAttachment $attachment){
        
        $photo_data[owner_id] = $attachment->owner_id();
        $photo_data[id] = $attachment->id();
        $photo = new VKPhoto($photo_data);
        return $photo;
        
    }*/

    public function get_string() {
        return $this->type() . parent::get_string();
    }
    
    public function type() {
        return VKAttachment::PHOTO;
    }
}
