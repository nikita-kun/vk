<?php

namespace SocialAutomation\VK;

//base class
class VKAttachment {

    const PHOTO = "photo";
    const POST = "post";
    const POST_WALL = "wall";
    const AUDIO = "audio";
    const VIDEO = "video";
    const DOCUMENT = "doc";
    
    private $id;
    private $owner;
    private $hash;
    protected $content_hash;

    public function __construct($owner_id, $id) {

        $this->id = (int) $id;
        $this->owner = new VKOwner($owner_id);
        $this->hash = substr(md5($this->get_string()), 0, 8);
        $this->content_hash = NULL;
        //echo "construct ".self::get_string()."<br>";
    }

    public function id() {
        return $this->id;
    }

    public function owner_id() {
        return $this->owner->id();
    }

    public function owner() {
        return $this->owner;
    }

    public function hash() {
        return $this->hash;
    }
    
    public function content_hash() {
        return $this->content_hash;
    }

    public function get_string() {
        return $this->owner->id() . "_" . $this->id;
    }

    public function get_string_without_prefix() {
        return $this->owner->id() . "_" . $this->id;
    }
    
    public function url(){
        return "https://vk.com/".$this->get_string();
    }
    
    public function url_html(){
        return "<a target='_blank' href='".$this->url()."'>".$this->get_string()."</a>";
    }
    
    //TODO: FIX DATA ISSUE
    public function data(){
        
        $data->owner_id = $this->owner_id();
        $data->id = $this->id;
        
        return $data;
    }
    
    //must be defined in derived classes
    public function type(){
        return NULL;
    }
    
    public function type_like(){
        return $this->type();
    }

}
