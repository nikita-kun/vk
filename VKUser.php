<?php

namespace SocialAutomation\VK;

class VKUser extends VKOwner {

    const GENDER_FEMALE = 1;
    const GENDER_MALE = 2;
    const GENDER_UNSPECIFIED = 0;
    const HAS_PHOTO = 1;
    const DONT_HAS_PHOTO = 0;
    const ONLINE = 1;
    const OFFLINE = 0;

    private $first_name;
    private $last_name;
    private $deactivated;
    private $sex;
    private $counters;
    private $can_write_private_message;
    private $can_post;
    private $has_photo;
    private $wall_comments;
    private $movies;
    private $music;
    private $quotes;
    private $exports;

    public function __construct($uid) {

        parent::__construct($uid);
        VKDebug::debug_construct($this, parent::id());
    }

    //fetch User associated object 
    public function fetch($data) {
        $this->first_name = $data->first_name;
        $this->last_name = $data->last_name;
        $this->deactivated = $data->deactivated;
        $this->sex = $data->sex;
        $this->counters = $data->counters;
        //can_write_private_message,can_post,has_photo,wall_comments,movies,music,quotes,exports
        $this->can_write_private_message = (bool) $data->can_write_private_message;
        $this->can_post = (bool) $data->can_post;
        $this->has_photo = (bool) $data->has_photo;
        $this->wall_comments = (bool) $data->wall_comments;
        $this->movies = $data->movies;
        $this->music = $data->music;
        $this->quotes = $data->quotes;
        $this->exports = $data->exports;
        var_dump($this);
    }

    public function first_name() {
        return $this->first_name;
    }

    public function last_name() {
        return $this->last_name;
    }

    public function is_male() {
        return $this->sex == self::GENDER_MALE;
    }

    public function is_female() {
        return $this->sex == self::GENDER_FEMALE;
    }

    public function friends_count() {
        return $this->counters->friends;
    }

    public function followers_count() {
        return $this->counters->followers;
    }

    public function is_deactivated() {
        return $this->deactivated != NULL;
    }

    public function is_banned() {
        return $this->deactivated == "banned";
    }

    public function is_deleted() {
        return $this->deactivated == "deleted";
    }

}
