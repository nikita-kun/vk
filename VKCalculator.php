<?php

namespace SocialAutomation\VK;

abstract class VKCalculator {

    public static function interval_setter_alpha($time = NULL) {

        if ($time == NULL) {
            $time = time();
        }

        if ($time < strtotime('00:28')) {
            return 18;
        }
        if ($time < strtotime('00:58')) {
            return 20;
        }
        if ($time < strtotime('01:58')) {
            return 30;
        }
        if ($time < strtotime('02:54')) {
            return 40;
        }
        if ($time < strtotime('06:00')) {
            return 1000;
        }

        if ($time < strtotime('09:00')) {
            return 300;
        }
        if ($time < strtotime('10:00')) {
            return 60;
        }

        if ($time < strtotime('10:50')) {
            return 50;
        }
        if ($time < strtotime('16:30')) {
            return 30;
        }
        if ($time < strtotime('16:58')) {
            return 28;
        }

        if ($time < strtotime('17:24')) {
            return 26;
        }

        if ($time < strtotime('17:48')) {
            return 24;
        }
        if ($time < strtotime('18:10')) {
            return 22;
        }
        if ($time < strtotime('18:30')) {
            return 20;
        }
        if ($time < strtotime('18:48')) {
            return 18;
        }
        if ($time < strtotime('19:20')) {
            return 16;
        }
        if ($time < strtotime('20:30')) {
            return 14;
        }
        if ($time < strtotime('20:54')) {
            return 12;
        }
        if ($time < strtotime('21:34')) {
            return 10;
        }
        if ($time < strtotime('22:00')) {
            return 12;
        }
        if ($time < strtotime('22:30')) {
            return 14;
        }

        return 18;
    }

    public static function interval_setter_alpha_seconds($time = NULL) {
        return self::interval_setter_alpha($time) * 60;
    }

}
