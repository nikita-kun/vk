<?php

namespace SocialAutomation\VK;

//Implementation of a fast and reliable history 
//Relies on MySQL PDO 
//History table is created automatically
class VKSQLHistory extends VKHistory {

    //some high limit
    const MAX_HISTORY_LENGTH = 10000000;
    const SQL_TABLE_ALREADY_EXISTS = '42S01';
    const SQL_INTEGRITY_CONSTRAINT_VIOLATION = '23000';

    //PDO handler
    private $dbh;

    public function __construct(VKSQLSetup $setup, $type, $max_length = VKSQLHistory::MAX_HISTORY_LENGTH, $id = "app") {

        VKDebug::debug_construct($this, "type#$type", "id#$id", "max_length#$max_length");

        //get a new connection handler
        $this->dbh = $setup->connect();

        //type property check
        if ($type < self::MIN_HISTORY_TYPE || $type > self::MAX_HISTORY_TYPE) {
            throw new \Exception("Unknown history type");
        }

        //max_length property check
        $this->max_length = (int) $max_length;
        if ($this->max_length <= 0 || $this->max_length > self::MAX_HISTORY_LENGTH) {
            throw new \Exception("Wrong history size length");
        }

        //setting the properties
        $this->type = (int) $type;
        $this->id = $id;

        //prepare a query to create a table
        $sql = $this->get_create_table_sql();

        try {
            //creating the table
            $this->dbh->exec($sql);
        } catch (\PDOException $e) {

            //rethrow the exception if it is not
            //42S01 -> TABLE_ALREADY_EXISTS
            if ($e->getCode() != self::SQL_TABLE_ALREADY_EXISTS) {
                throw $e;
            }
        }

        $this->forget_all_obsolete();
    }
    
    private function get_create_table_sql(){
        
        if ($this->type == VKHistory::LOG){
            return "CREATE TABLE `" . $this->table_name() . "` (
            `key` text NOT NULL,
            `value` text,
            `timestamp` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            KEY `timestamp` (`timestamp`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        }
        
        return "CREATE TABLE `" . $this->table_name() . "` (
            `key` tinytext NOT NULL,
            `value` tinytext,
            `timestamp` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`key`(32)),
            KEY `timestamp` (`timestamp`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
    }

    //generate a table name from id and type
    public function table_name() {
        return $this->id . "_" . $this->type;
    }

    //don't need to do anything with sql
    public function pull() {
        //do nothing
    }

    //don't need to do anything with sql
    public function push() {
        //do nothing
    }

    //write the value to database
    //try to insert the value
    //in case of key duplication update the value
    public function write($key, $value = NULL) {

        VKDebug::debug_function(__METHOD__, $key, $value);

        //prepare a statement to insert the key/value pair
        $sql = "INSERT INTO " . $this->table_name() . " 
                (`key`, `value`) VALUES (" .
                $this->dbh->quote($key) . "," .
                $this->dbh->quote($value) . "
                )";


        try {
            $this->dbh->query($sql);
        } catch (\PDOException $e) {
            //rethrow exception if there is something wrong
            //and it is not because of a key duplication
            //otherwise suppress the exception
            if ($e->getCode() != self::SQL_INTEGRITY_CONSTRAINT_VIOLATION) {
                throw $e;
            } else {
                //update value
                $this->update($key, $value);
            }
        }
    }

    private function update($key, $value) {

        VKDebug::debug_function(__METHOD__, $key, $value);

        //prepare a statement to insert the key/value pair
        $sql = "UPDATE " . $this->table_name() . " 
                SET `value` = " .
                $this->dbh->quote($value) . " 
                WHERE `key` = " .
                $this->dbh->quote($key);



        $this->dbh->query($sql);
    }
    
    public function delete($key) {

        VKDebug::debug_function(__METHOD__, $key);

        //prepare a statement to insert the key/value pair
        $sql = "DELETE FROM " . $this->table_name() . " 
                WHERE `key` = " .
                $this->dbh->quote($key);

        $this->dbh->query($sql);
    }

    //just an alias in this implementation
    public function write_and_push($key, $value = NULL) {
        $this->write($key, $value);
    }

    //return the value corrensponging to key
    public function value($key) {

        VKDebug::debug_function(__METHOD__, $key);

        //Prepare a statement
        $sql = "SELECT value from "
                . "" . $this->table_name() . " WHERE `key` = " .
                $this->dbh->quote($key);

        //execute and get the result
        $result = $this->dbh->query($sql);
        $row = $result->fetch();

        VKDebug::debug_retval(__METHOD__, mb_substr($row['value'], 0, 15)."...");
        return $row['value'];
    }

    //don't need to do anything
    public function forget_one_obsolete() {
        //do nothing
    }

    //remove n oldest records where n is (rows - max_size) 
    //if there are more rows in database than needed
    //The result is not guaranteed
    public function forget_all_obsolete() {

        //transaction begins
        $this->dbh->beginTransaction();

        //current history size
        $currentLength = $this->size();

        if ($currentLength <= $this->max_length) {
            $this->dbh->commit();
            return;
        }

        $sql = "DELETE FROM " . $this->table_name() .
                " ORDER BY timestamp ASC LIMIT " .
                ($currentLength - $this->max_length);


        //execute and get the result
        $this->dbh->exec($sql);

        if ($this->size() == $this->max_length) {
            $this->dbh->commit();
        } else {
            $this->dbh->rollBack();
        }
    }

    //return the first key
    public function first() {

        //prepare a statement to get the newest row
        $sql = "SELECT `key` from " . $this->table_name() . " ORDER BY timestamp DESC LIMIT 1";

        //execute and get the key
        $result = $this->dbh->query($sql);
        $row = $result->fetch();

        VKDebug::debug_retval(__METHOD__, $row['key']);

        return $row['key'];
    }
    
    //return the first key
    public function first_like($key = NULL, $value = NULL) {

        //prepare a statement to get the newest row
        $sql = "SELECT `key` from " . $this->table_name() . 
                (($key || $value) ? " WHERE " : "").
                ($key ? "  `key` LIKE ".str_replace('\%', '%', $this->dbh->quote($key)) : "").
                (($key && $value) ? " AND " : "").
                ($value ? " `value` LIKE ".str_replace('\%', '%', $this->dbh->quote($value)): "").
                " ORDER BY timestamp DESC LIMIT 1";

        //execute and get the key
        $result = $this->dbh->query($sql);
        $row = $result->fetch();

        VKDebug::debug_retval(__METHOD__, $row['key']);

        return $row['key'];
    }
    
    //return the first key
    public function first_value_like($key = NULL, $value = NULL) {

        //prepare a statement to get the newest row
        $sql = "SELECT `value` from " . $this->table_name() . 
                (($key || $value) ? " WHERE " : "").
                ($key ? "  `key` LIKE ".str_replace('\%', '%', $this->dbh->quote($key)) : "").
                (($key && $value) ? " AND " : "").
                ($value ? " `value` LIKE ".str_replace('\%', '%', $this->dbh->quote($value)): "").
                " ORDER BY timestamp DESC LIMIT 1";

        //execute and get the key
        $result = $this->dbh->query($sql);
        $row = $result->fetch();

        VKDebug::debug_retval(__METHOD__, $row['value']);

        return $row['value'];
    }
    
    //return the first key
    public function first_timestamp_like($key = NULL, $value = NULL) {

        //prepare a statement to get the newest row
        $sql = "SELECT `timestamp` from " . $this->table_name() . 
                (($key || $value) ? " WHERE " : "").
                ($key ? "  `key` LIKE ".str_replace('\%', '%', $this->dbh->quote($key)) : "").
                (($key && $value) ? " AND " : "").
                ($value ? " `value` LIKE ".str_replace('\%', '%', $this->dbh->quote($value)): "").
                " ORDER BY timestamp DESC LIMIT 1";

        //execute and get the key
        $result = $this->dbh->query($sql);
        $row = $result->fetch();

        VKDebug::debug_retval(__METHOD__, $row['timestamp']);

        return $row['timestamp'];
    }
    
    public function first_value() {

        //prepare a statement to get the newest row
        $sql = "SELECT `value` from " . $this->table_name() . " ORDER BY timestamp DESC LIMIT 1";

        //execute and get the key
        $result = $this->dbh->query($sql);
        $row = $result->fetch();

        VKDebug::debug_retval(__METHOD__, $row['value']);

        return $row['value'];
    }
    
    //return the first key
    //OBSOLETE NAME 
    //TBD REMOVE
    public function get_last_timestamp() {

        //prepare a statement to get the newest row
        $sql = "SELECT `timestamp` from " . $this->table_name() . " ORDER BY timestamp DESC LIMIT 1";

        //execute and get the key
        $result = $this->dbh->query($sql);
        $row = $result->fetch();

        VKDebug::debug_retval(__METHOD__, $row['timestamp']);

        return $row['timestamp'];
    }
    
    //fix confusing names
    public function first_timestamp() {
        return $this->get_last_timestamp();
    }

    //check whether the table contains a row with required key
    //TODO use value function to shorten the code
    public function contains($key) {

        //Prepare a statement
        $sql = "SELECT count(1) as rows from "
                . "" . $this->table_name() . " WHERE `key` = " .
                $this->dbh->quote($key);

        //execute and get the result
        $result = $this->dbh->query($sql);
        $row = $result->fetch();

        return $row['rows'] == 1;
    }

    //check whether the table contains an attachment
    public function contains_attachment(VKAttachment $attachment) {
        return $this->contains($attachment->get_string());
    }

    //getting the updating the amount of rows
    //mostly useless method for this implementation
    //NOT thread-safe
    public function size() {

        VKDebug::debug_function(__METHOD__);

        //Prepare a statement
        $result = $this->dbh->query("SELECT count(1) as rows from " . $this->table_name());

        //execute and get the result
        $row = $result->fetch();
        $this->length = (int) $row['rows'];

        VKDebug::debug_retval(__METHOD__, $this->length);

        return $this->length;
    }

}
