<?php

namespace SocialAutomation\VK;

class VKGroup extends VKOwner {

    private $title;

    public function __construct($id, $title = NULL) {
        $group_id = (int) $id;

        $this->title = $title;

        parent::__construct($group_id);

        VKDebug::debug_construct($this, $group_id, $title);
    }

    public function id() {
        return "-" . parent::id();
    }

    public function unsigned_id() {
        return parent::id();
    }

    public function title() {
        return $this->title;
    }
    
    public function date_id() {
        return date("Y-m-d") . " " . $this->title;
    }

}
