<?php

namespace SocialAutomation\VK;

class VKVideo extends VKAttachment {

    public function __construct($video) {

        parent::__construct($video->owner_id, $video->id);

        VKDebug::debug_construct($this, $this->get_string());
    }

    public function get_string() {
        return $this->type() . parent::get_string();
    }
    
    public function type() {
        return VKAttachment::VIDEO;
    }

}
