<?php

namespace SocialAutomation\VK;

class VKAudio extends VKAttachment {

    private $artist;
    private $title;
    private $duration;
    private $date;
    private $url;
    private $genre_id;
    private $lyrics_id;
    private $hash;

    //basic requirements are
    const REQUIREMENT_MAX_DURATION = 420;
    const REQUIREMENT_MIN_DURATION = 130;
    const REQUIRE_IGNORE_RAP = true;

    public function __construct($audio) {
        parent::__construct($audio->owner_id, $audio->id);
        $this->artist = $audio->artist;
        $this->title = $audio->title;
        $this->duration = (int) $audio->duration;
        $this->date = (int) $audio->date;
        $this->url = $audio->url;
        $this->genre_id = (int) $audio->genre_id;
        $this->lyrics_id = (int) $audio->lyrics_id;
        $this->hash = VKAudio::calculate_hash($audio->artist, $audio->title);

        VKDebug::debug_construct($this, $this->get_string(), $this->artist, $this->title);
    }

    public static function calculate_hash($artist, $title) {
        $hash = substr(md5($artist . $title), 0, 8);
        return $hash;
    }

    public function meet_basic_requirements() {

        if ($this->duration > self::REQUIREMENT_MAX_DURATION) {
            return false;
        }

        if ($this->duration < self::REQUIREMENT_MIN_DURATION) {
            return false;
        }

        if (self::REQUIRE_IGNORE_RAP && $this->genre_id == VKGenre::RAP_HIPHOP) {
            return false;
        }

        return true;
    }

    public function artist() {
        return $this->artist;
    }

    public function title() {
        return $this->title;
    }

    public function hash() {
        return $this->hash;
    }

     public function get_string() {
        return $this->type() . parent::get_string();
    }
    
    public function type() {
        return VKAttachment::AUDIO;
    }
    
}
