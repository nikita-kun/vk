<?php

namespace SocialAutomation\VK;

class VKGenre {

    const ROCK = 1;
    const POP = 2;
    const RAP_HIPHOP = 3;
    const EASY_LISTENING = 4;
    const DANCE_HOUSE = 5;
    const INSTRUMENTAL = 6;
    const METAL = 7;
    const DUBSTEP = 8;
    const JAZZ_BLUEZ = 9;
    const DRUM_AND_BASS = 10;
    const TRANCE = 11;
    const CHANSON = 12;
    const ETHNIC = 13;
    const ACOUSTIC_VOCAL = 14;
    const RAGGAE = 15;
    const CLASSICAL = 16;
    const INDIE_POP = 17;
    const OTHER = 18;
    const SPEECH = 19;
    const ALTERNATIVE = 21;
    const ELECTROPOP_DISCO = 22;

    public static function pop() {


        $genre = self::EASY_LISTENING;

        if (rand(0, 100) >= 50) {
            $genre = self::POP;
        }

        if (rand(0, 100) >= 90) {
            $genre = self::DANCE_HOUSE;
        }

        return $genre;
    }

}

/************** GENRE LIST  ***************
1	Rock
2	Pop
3	Rap & Hip-Hop
4	Easy Listening
5	Dance & House
6	Instrumental
7	Metal
21	Alternative
8	Dubstep
9	Jazz & Blues
10	Drum & Bass
11	Trance
12	Chanson
13	Ethnic
14	Acoustic & Vocal
15	Reggae
16	Classical
17	Indie Pop
19	Speech
22	Electropop & Disco
18	Other
************* GENRE LIST  ****************/
	