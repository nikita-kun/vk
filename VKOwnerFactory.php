<?php

namespace SocialAutomation\VK;

abstract class VKOwnerFactory {

    //create either a VKUser or VKGroup instance by checking identificator's sign
    public static function from_id($owner_id) {
    
        $id = (int) $owner_id;

        if (!$id){
            throw new \Exception("Unable to instance owner without id");
        } else
        if ($id < 0) {
            //it is a group
            return new VKGroup($id);
        } else {
            //it is a user
            return new VKUser($id);
        }
    }


}
