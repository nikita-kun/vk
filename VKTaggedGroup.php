<?php

namespace SocialAutomation\VK;

class VKTaggedGroup extends VKGroup {

    private $tags;
    
    public function __construct($id, $title = NULL, $tags = NULL) {

        parent::__construct($id, $title);

        $this->tags = $tags;
        
        VKDebug::debug_construct($this, $id, $title, $tags);
    }
    
    public function tag_string(){
        return $this->tags;
    }


}
