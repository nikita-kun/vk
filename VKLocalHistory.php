<?php

namespace SocialAutomation\VK;

//VKHistory Filsystem Realisation
//Slow and unreliable, but the choice in case of a lack of a DB
//keep in mind that only one VKHistory instance of its type could be instanced per one VK app (id, appname)

class VKLocalHistory extends VKHistory {

    //max length for the history 
    //of a kind
    //{"28c4c3b9":true,"7a2dd0bb":true, ... , "11c4c3b9":true,"332dd0bb":true}
    const MAX_HISTORY_LENGTH = 10000;

    private $storage_path;

    public function __construct($type, $max_length, $storage_path, $id = "app") {

        VKDebug::debug_construct($this, "type#$type", "id#$id", "max_length#$max_length", "path = $storage_path");

        if ($type < self::MIN_HISTORY_TYPE || $type > self::MAX_HISTORY_TYPE) {
            throw new \Exception("Unknown history type");
        }

        $this->max_length = (int) $max_length;
        if ($this->max_length <= 0 || $this->max_length > self::MAX_HISTORY_LENGTH) {
            throw new \Exception("Wrong history size length");
        }

        $this->type = (int) $type;
        $this->length = (int) $max_length;
        $this->id = $id;
        $this->storage_path = $storage_path;
    }

    public function pull() {

        VKDebug::debug_function(__METHOD__);

        $raw = file_get_contents($this->storage_path . "/history_id" . $this->id() . "_type" . $this->type());
        if ($raw === false) {
            throw new \Exception("Unable to load history");
        }
        $history = json_decode($raw, true); //decode as an associative array

        if (is_array($history)) {
            $this->history = $history;
        }
        $this->length = sizeof($history);

        VKDebug::debug_retval(__METHOD__, "$raw [$this->length]");

        return $this->history;
    }

    public function push() {
        VKDebug::debug_function(__METHOD__);

        file_put_contents($this->storage_path . "/history_id" . $this->id() . "_type" . $this->type(), json_encode($this->history));
    }

}
