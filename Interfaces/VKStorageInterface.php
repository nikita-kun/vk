<?php

namespace SocialAutomation\VK\Interfaces;

interface VKStorageInterface{
    public function set($value);
    public function get();
}
