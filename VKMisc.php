<?php

namespace SocialAutomation\VK;

class VKMisc {

    
    public static function read_binary($url) {

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $raw = curl_exec($ch);
        curl_close($ch);

        return $raw;
    }
    
    public static function url_follow_redirect($url, $redirections = 4, $trafficLimit = 1024){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_RANGE,"1-".$trafficLimit);

         
        $raw = curl_exec($ch);
        //var_dump($raw);
        preg_match_all('/^Location:(.*)$/mi', $raw, $matches);
        curl_close($ch);
               
        if ($redirections < 0) {
            return NULL;
        }
        
        return !empty($matches[1]) ? self::url_follow_redirect(trim($matches[1][0]), $redirections - 1) : $url;

    }
    
    public static function read_binary_range($url, $start, $end) {

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($ch, CURLOPT_RANGE, "$start-$end");
        
        $raw = curl_exec($ch);
        
        curl_close($ch);

        return $raw;
    }
    
    public static function middle_hash($url, $filesize){
        
        if ($filesize == 0){
            return md5("");
        }
        
        $middleAddress = (int)($filesize / 2);
        
        $start = $middleAddress > 1024 ? $middleAddress - 1024 : 0;
        $end = $middleAddress + 1024 < $filesize ? $middleAddress + 1024 : $filesize - 1;

        return md5(self::read_binary_range($url, $start, $end).$filesize);
        
    }

    public static function remote_filesize($url) {
        static $regex = '/^Content-Length: *+\K\d++$/im';
        if (!$fp = @fopen($url, 'rb')) {
            return false;
        }
        if (
                isset($http_response_header) &&
                preg_match($regex, implode("\n", $http_response_header), $matches)
        ) {
            return (int) $matches[0];
        }
        return strlen(stream_get_contents($fp));
    }

    public static function get_http($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    public static function get_json($url) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }

    public static function post_files($files, $upload_url) {

        $ch = curl_init($upload_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, "");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $files);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }

    public static function tempfile_from_url($url, $dir, $extension = ".gif") {
        $ch = curl_init();

        $tmp = tempnam($dir . "/tmp/", "FOO") . $extension;
        $file = fopen($tmp, "w");

        fwrite($file, VKMisc::read_binary($url));
        fclose($file);

        return $tmp;
    }

    public static function get_random_tags_from_string($string, $number_of_tags, $extra_words = NULL) {

        preg_match_all("/[аi-яi]{15,}|[A-Za-z]{3,}/", $string, $matches);

        $words = array_unique($matches[0]);
        shuffle($words);

        $tags = "";
        $counter = 0;
        foreach ($words as $match) {
            $tags .= " #" . $match;
            if (++$counter == $number_of_tags) {
                break;
            }
        }

        foreach ($extra_words as $word) {
            $tags .= " #" . $word;
            if (++$counter == $number_of_tags) {
                break;
            }
        }

        return $tags;
    }

}
