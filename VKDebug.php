<?php

namespace SocialAutomation\VK;

abstract class VKDebug {

    public static function disable() {
        $GLOBALS["VKDebug"] = false;
        error_reporting(0);
    }

    public static function enable() {
        $GLOBALS["VKDebug"] = true;
        error_reporting(E_ALL ^ E_NOTICE);
    }

    public static function disable_api() {
        $GLOBALS["VKAPIDebug"] = false;
    }

    public static function enable_api() {
        $GLOBALS["VKAPIDebug"] = true;
    }

    public static function debug($message) {

        if ($GLOBALS["VKDebug"] == true) {

            try {
                echo $message . "<br>";
            } catch (\Exception $ex) {
                echo $ex->getMessage();
            }
        }
    }

    public static function debug_list($message) {

        if ($GLOBALS["VKDebug"] == true) {

            try {
                echo $message . " / ";
                $arguments = func_get_args();

                foreach ($arguments as $arg) {
                    if ($arg == $message) {
                        continue;
                    }
                    echo $arg . ", ";
                }

                echo "<br>";
            } catch (\Exception $ex) {
                echo $ex->getMessage();
            }
        }
    }

    public static function debug_construct($object) {

        if ($GLOBALS["VKDebug"] == true) {

            try {
                $output = "<b><i>new " . get_class($object) . " </i>( ";
                $arguments = func_get_args();

                foreach ($arguments as $arg) {
                    if ($arg === $object) {
                        continue;
                    }
                    $output .= $arg . ", ";
                }

                $output = substr($output, 0, -2);

                $output .= " )</b><br>";
                echo $output;
            } catch (\Exception $ex) {
                echo $ex->getMessage();
            }
        }
    }

    public static function debug_function($function_name) {

        if ($GLOBALS["VKDebug"] == true) {

            try {

                $output = "<b><i>$function_name </i>(   ";
                $arguments = func_get_args();
                array_shift($arguments);

                foreach ($arguments as $arg) {
                    if (is_string($arg) || is_numeric($arg)) {
                        $output .= $arg . ", ";
                    }
                }

                $output = substr($output, 0, -2);

                $output .= " )</b><br>";
                echo $output;
            } catch (\Exception $ex) {
                echo $ex->getMessage();
            }
        }
    }

    public static function debug_retval($function_name) {

        if ($GLOBALS["VKDebug"] == true) {

            try {

                $output = "<b>$function_name (  ";
                $arguments = func_get_args();
                array_shift($arguments);
                $retval = array_pop($arguments);

                foreach ($arguments as $arg) {
                    $output .= $arg . ", ";
                }

                $output = substr($output, 0, -2);

                $output .= " )</b> -> <font color=green>$retval</font><br>";
                echo $output;
            } catch (\Exception $ex) {
                echo $ex->getMessage();
            }
        }
    }

    public static function debug_api($function_name) {

        if ($GLOBALS["VKAPIDebug"] == true) {

            try {

                $output = "<b>$function_name (  ";
                $arguments = func_get_args();
                array_shift($arguments);
                $retval = array_pop($arguments);

                foreach ($arguments as $arg) {
                    $output .= $arg . ", ";
                }

                $output = substr($output, 0, -2);

                $output .= " )</b> -> <font size=2>$retval</font><br>";
                echo $output;
            } catch (\Exception $ex) {
                echo $ex->getMessage();
            }
        }
    }

    public static function debug_execute($message, $call) {
        if ($GLOBALS["VKDebug"] == true) {

            try {
                $call();
                echo $message . "<br>";
            } catch (\Exception $ex) {
                echo $ex->getMessage();
            }
        }
    }

}
