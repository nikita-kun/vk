<?php

namespace SocialAutomation\VK;

class VKLink extends VKAttachment {

    private $title;
    private $url;

    public function __construct($link) {

        $this->title = $link->title;
        $this->url = $link->url;

        VKDebug::debug_construct($this, $this->title, $this->url);
    }

    public function get_string() {
        return $this->url;
    }

    public function url() {
        return $this->url;
    }

    public function title() {
        return $this->title;
    }

    public function hash() {
        return substr(md5($this->get_string()), 0, 8);
    }

}
