<?php

namespace SocialAutomation\VK;

class VKAutopost {

    private $need_to_post = false;
    private $group;
    private $history_array = Array();
    private $post_interval;
    private $last_post_time;
    private $message;
    private $audios = Array();
    private $photos = Array();
    private $videos = Array();
    private $documents = Array();
    private $link;
    private $vk;

    public function __construct(VKGroup $group, $post_interval, VK $vk) {

        VKDebug::debug_construct($this, $group->title(), "post_interval#$this->post_interval minutes", "VK:vk");

        $this->group = $group;
        $this->post_interval = (int) $post_interval;
        $this->vk = $vk;

        //$this->init_history(VKHistory::POST_AUDIO, VKHistory::POST_AUDIO_DEFAULT_LENGTH, $group->id()); 
        //$this->init_history(VKHistory::BROADCAST_AUDIO, VKHistory::BROADCAST_AUDIO_DEFAULT_LENGTH, $group->id());
        //$this->init_history(VKHistory::LAST_POST, VKHistory::LAST_POST_DEFAULT_LENGTH, $group->id());

        $this->last_post_time = (int) ($this->vk->global_storage_get("last_post_" . $this->get_group_id()));

        //set the 'need_to_post' flag if enough time has elapsed
        if ((time() - $this->last_post_time) >= $this->post_interval * 60) {
            $this->need_to_post = true;
        }
    }

    public function set_history($type, $history) {

        VKDebug::debug_function(__METHOD__, "type#$type");
        $this->history_array[$type] = $history;
        $this->history_array[$type]->pull();

        return $this->history_array[$type];
    }

    public function init_history($type, $max_length, $id = "app") {

        VKDebug::debug_function(__METHOD__, "type#$type", "length#$max_length", "id#$id");
        $this->history_array[$type] = new VKHistory($type, $max_length, $this->vk, $id);
        $this->history_array[$type]->pull();

        return $this->history_array[$type];
    }

    public function init_local_history($type, $max_length, $id = "app") {
        VKDebug::debug_function(__METHOD__, "type#$type", "length#$max_length", "id#$id");

        $this->history_array[$type] = new VKLocalHistory($type, $max_length, $this->vk->tmpdir(), $id);
        $this->history_array[$type]->pull();

        return $this->history_array[$type];
    }

    public function need_to_post() {
        VKDebug::debug_retval(__METHOD__, (int) $this->need_to_post);

        return $this->need_to_post;
    }

    public function get_group() {
        return $this->group;
    }

    public function attach(VKAttachment $attachment) {

        switch (get_class($attachment)) {
            case "SocialAutomation\VK\VKAudio": array_push($this->audios, $attachment);
                break;
            case "SocialAutomation\VK\VKVideo": array_push($this->videos, $attachment);
                break;
            case "SocialAutomation\VK\VKPhoto": array_push($this->photos, $attachment);
                break;
            case "SocialAutomation\VK\VKDocument": array_push($this->documents, $attachment);
                break;
            case "SocialAutomation\VK\VKLink": $this->link = $attachment;
                break;
            default: throw new \Exception("Unsupported attachment");
        }

        VKDebug::debug_function(__METHOD__, $attachment->get_string());

        return $attachment;
    }

    public function broadcast() {
        VKDebug::debug_function(__METHOD__);

        if (sizeof($this->audios) == 0) {
            throw new \Exception("No audio to broadcast");
        }

        $result = $this->broadcast_audio($this->audios[0]);

        return $result;
    }

    public function get_group_id() {

        $result = $this->group->id();

        return $result;
    }

    public function broadcast_audio(VKAudio $audio) {

        VKDebug::debug_function(__METHOD__, $audio->get_string(), $audio->artist(), $audio->title());

        $broadcasted_to = $this->vk->audio_set_broadcast_to_owner($audio, $this->group);

        $history = $this->get_history(VKHistory::BROADCAST_AUDIO);

        $history->write_attachment($audio);
        $history->push();

        return $broadcasted_to;
    }

    public function get_history($type) {

        VKDebug::debug_function(__METHOD__, "type#$type");

        if ($type < VKHistory::MIN_HISTORY_TYPE || $type > VKHistory::MAX_HISTORY_TYPE) {
            throw new \Exception("Unknown history type");
        }


        return $this->history_array[$type];
    }

    public function get_fresh_audio($audio_array, VKHistory $history) {

        VKDebug::debug_function(__METHOD__, "VKAudio array[" . sizeof($audio_array) . "]", "VKHistory::history");

        if (!shuffle($audio_array)) {
            throw new \Exception("Error shuffling audios");
        }

        foreach ($audio_array as $audio) {

            if (!$history->contains_attachment($audio)) {
                VKDebug::debug_retval(__METHOD__, "VKAudio array[" . sizeof($audio_array) . "]", "VKHistory::history", $audio->artist(), $audio->title());

                return $audio;
            }
        }

        throw new \Exception("No fresh audio available");
    }

    public function set_message($message) {
        VKDebug::debug_function(__METHOD__, $message);

        $this->message = $message;

        return $this->message;
    }

    public function get_message() {
        VKDebug::debug_retval(__METHOD__, $this->message);
        return $this->message;
    }

    public function set_link(VKLink $link) {

        VKDebug::debug_function(__METHOD__, $link->get_string());
        $this->link = $link;
        return $this->link;
    }

    public function get_link() {

        VKDebug::debug_retval(__METHOD__, $this->link);
        return $this->link;
    }

    public function get_attachments() {

        $links = Array();
        if ($this->link) {
            array_push($links, $this->link);
        }
        return array_merge($this->audios, $this->photos, $this->videos, $this->documents, $links);
    }

    public function attachment_history_commit() {

        VKDebug::debug_retval(__METHOD__);

        $history = $this->get_history(VKHistory::ATTACHMENTS);
        if ($history) {
            foreach ($this->audios as $audio) {
                $history->write_attachment($audio);
            }
            foreach ($this->videos as $video) {
                $history->write_attachment($video);
            }
            foreach ($this->photos as $photo) {
                $history->write_attachment($photo);
            }

            foreach ($this->documents as $document) {
                $history->write_attachment($document);
            }

            $history->push();
        }
    }

    public function post() {
        VKDebug::debug_retval(__METHOD__);

        $this->attachment_history_commit();
        $this->vk->global_storage_set("last_post_" . $this->get_group_id(), time());

        $result = $this->vk->wall_post($this->get_group(), $this->message, $this->get_attachments());

        return $result;
    }

}
