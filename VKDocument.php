<?php

namespace SocialAutomation\VK;

class VKDocument extends VKAttachment {

    private $title;
    private $size;
    private $extension;
    private $url;
    private $date;
    private $video_preview;

    public function __construct($document) {

        parent::__construct($document->owner_id, $document->id);
        
        $this->title = $document->title;
        $this->size = (int) $document->size;
        $this->extension = $document->ext;
        $this->url = $document->url;
        $this->date = (int) $document->date;
        $this->video_preview = $document->preview->video;

        VKDebug::debug_construct($this, $this->get_string(), $this->title, $this->extension, "$this->size Bytes");
    }

    public function title() {
        return $this->title;
    }

    public function size() {
        return $this->size;
    }

    public function url() {
        return $this->url;
    }
    
    public function content_hash() {
        if (!$this->content_hash){
           // $this->content_hash = $this->calculate_content_hash();
        }
    }
    

    public function extension() {
        return $this->extension;
    }

    public function date() {
        return $this->date;
    }

    public function has_video_preview() {
        return $this->video_preview != NULL && $this->video_preview->file_size > 0;
    }
    
    public function video_preview_height(){
        return (int)$this->video_preview->height;
    }
    
    public function video_preview_width(){
        return (int)$this->video_preview->width;
    }
    
    public function video_preview_aspect_ratio(){
        if ($this->video_preview->height > 0 && $this->video_preview->width > 0){
            return $this->video_preview->width / $this->video_preview->height;
        }
        return NULL;
    }
    
     public function video_preview_file_size(){
        return (int)$this->video_preview->file_size;
    }

    public function get_string() {
        return "doc" . parent::get_string();
    }

}
