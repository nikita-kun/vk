<?php

namespace SocialAutomation\VK;

class VKSQLSetup {

    private $username;
    private $password;
    private $host;
    private $dbname;
    
    private $dbh;

    //safe parameters expected
    public function __construct($host, $dbname, $user, $password) {
        $this->username = $user;
        $this->password = $password;
        $this->host = $host;
        $this->dbname = $dbname;
    }

   
    //return a PDO database connection handler
    //create new handler if it is not there
    public function connect() {     
        
        //create a new handler if it does not exist
        if (!$this->dbh){
            //prepare
            $dsn = "mysql:host=$this->host;dbname=$this->dbname";

            //setting the options
            $options = array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
            );

            //connecting to database
            $this->dbh = new \PDO($dsn, $this->username, $this->password, $options);
        }
        
        //returning the handler
        return $this->dbh;
    }

}
