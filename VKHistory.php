<?php

namespace SocialAutomation\VK;

//VKHistory v1.0 with 2 history types
//Update the MIN and MAX history type if the list is modified
//keep in mind that only one VKHistory instance of its type could be instanced in one VK app
class VKHistory {

    const POST_AUDIO = 1;
    const POST_AUDIO_DEFAULT_LENGTH = 50;
    const BROADCAST_AUDIO = 2;
    const BROADCAST_AUDIO_DEFAULT_LENGTH = 50;
    const LAST_POST = 3;
    const LAST_POST_DEFAULT_LENGTH = 1;
    const SOURCE_IMAGE = 4;
    const SOURCE_IMAGE_DEFAULT_LENGTH = 50;
    const PROFILE = 5;
    const PROFILE_DEFAULT_LENGTH = 50;
    const ATTACHMENTS = 6;
    const ATTACHMENTS_DEFAULT_LENGTH = 50;
    const OFFSET = 7;
    const OFFSET_LENGTH = 1;
    const VARIABLES = 8;
    const VARIABLES_LENGTH = 50;
    const OWNER = 9;
    const OWNER_DEFAULT_LENGTH = 50;
    const HASH = 10;
    const HASH_DEFAULT_LENGTH = 50;
    const LOG = 11;
    const LOG_DEFAULT_LENGTH = 50;
    //LIMITS
    const MIN_HISTORY_TYPE = 1;
    const MAX_HISTORY_TYPE = 11;
    //experimentally determined max length for the history 
    //with the following JSON format
    //{"28c4c3b9":true,"7a2dd0bb":true, ... , "11c4c3b9":true,"332dd0bb":true}
    const MAX_HISTORY_LENGTH = 51;

    protected $max_length;
    protected $history = Array();
    protected $id;
    protected $type;
    protected $vk;

    public function __construct($type, $max_length, VK $vk, $id = "app") {

        VKDebug::debug_construct($this, "type#$type", "id#$id", "max_length#$max_length", "VK::vk");

        if ($type < self::MIN_HISTORY_TYPE || $type > self::MAX_HISTORY_TYPE) {
            throw new \Exception("Unknown history type");
        }

        $this->max_length = (int) $max_length;
        if ($this->max_length <= 0 || $this->max_length > self::MAX_HISTORY_LENGTH) {
            throw new \Exception("Wrong history size length");
        }

        $this->type = (int) $type;
        $this->length = (int) $max_length;
        $this->vk = $vk;
        $this->id = $id;
    }

    public function pull() {

        VKDebug::debug_function(__METHOD__);

        $raw = $this->vk->global_storage_get("history_id" . $this->id() . "_type" . $this->type());
        $history = json_decode($raw, true); //decode as an associative array

        $this->history = $history;
        $this->length = sizeof($history);

        VKDebug::debug_retval(__METHOD__, "$raw [$this->length]");

        return $this->history;
    }

    public function push() {
        VKDebug::debug_function(__METHOD__);

        $this->vk->global_storage_set("history_id" . $this->id() . "_type" . $this->type(), json_encode($this->history));
    }

    public function write_attachment(VKAttachment $attachment, $value = NULL) {
        VKDebug::debug_function(__METHOD__, $attachment->get_string());

        $this->write($attachment->get_string(), $value);
    }

    public function value($key) {
        return $this->history[$key];
    }

    public function write($key, $value = NULL) {

        if ($value == NULL) {
            $value = date("Y-m-d H:m:s");
        }

        VKDebug::debug_function(__METHOD__, $key, $value);

        $this->history[$key] = $value;
        $this->forget_all_obsolete();
    }

    public function write_and_push($key) {
        $this->write($key);
        $this->push();
    }
    
    public function increment($key){
        $this->write($key, $this->value($key) + 1);
    }

    public function forget_one_obsolete() {

        $forgotten = current($this->history);
        unset($this->history[current(array_keys($this->history))]);
        $this->length = sizeof($this->history);

        return $forgotten;
    }

    public function forget_all_obsolete() {
        VKDebug::debug_function(__METHOD__);
        while (sizeof($this->history) > $this->max_length) {
            $this->forget_one_obsolete();
        }
    }

    public function first() {
        reset($this->history);
        $key = key($this->history);

        VKDebug::debug_retval(__METHOD__, $key);

        return $key;
    }

    public function contains($key) {
        return array_key_exists($key, $this->history);
    }

    public function contains_attachment(VKAttachment $attachment) {
        return array_key_exists($attachment->get_string(), $this->history);
    }

    public function type() {
        return $this->type;
    }

    public function id() {
        return $this->id;
    }

    public function size() {
        return sizeof($this->history);
    }

}
