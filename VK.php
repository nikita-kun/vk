<?php

namespace SocialAutomation\VK;

//disable debug in production
//VKDebug::disable();
//VKDebug::disable_api();
//class to work with VK.com API

class VK {

    const API_HISTORY_LENGTH = 30000;
    const CAPTCHA_HISTORY_LENGTH = 100000;
    const POST_HISTORY_LENGTH = 100000;

    private $access_token = NULL;
    private $sleep_microseconds = 2500000;
    private $tmpdir;
    private $connection;
    public $api_history;
    public $captcha_history;
    public $post_history;

    const SKIP_ACCESS_TOKEN = 0;
    const NEED_ACCESS_TOKEN = 1;
    const ENGLISH_ONLY = 1;
    const NOT_ONLY_ENGLISH = 0;
    const INIT_LAZY = 0;
    const INIT_INSTANT = 1;
    const PARAM_QUERY = NULL;
    const SORT_POPULAR = 0;
    const SORT_NEW_FIRST = 1;
    const SORT_MEMBER_ID_ASC = 'id_asc';
    const SORT_MEMBER_ID_DESC = 'id_desc';
    const SORT_MEMBER_TIME_ASC = 'time_asc';
    const SORT_MEMBER_TIME_DESC = 'time_desc';
    const MAX_USERS_PER_QUERY = 1000;

    public function __construct($tmpdir = "/tmp", $token = NULL, VKSQLSetup $connection = NULL) {

        VKDebug::debug_construct($this, $tmpdir);

        if ($token) {
            $this->set_access_token($token);
        }       

        if ($connection) {
            $this->connection = $connection;
            $this->api_history = new VKSQLHistory($connection, VKHistory::LOG, self::API_HISTORY_LENGTH, "api");
            $this->captcha_history = new VKSQLHistory($connection, VKHistory::LOG, self::CAPTCHA_HISTORY_LENGTH, "captcha");
            $this->post_history = new VKSQLHistory($connection, VKHistory::VARIABLES, self::POST_HISTORY_LENGTH, "post");
        } else {
            VKDebug::debug("No SQL connection for VK");
        }

        //TODO move srand to loader
        if (!$GLOBALS["RAND_INIT"] ++) {
            srand((double) microtime() * 1000000);
        }

        $this->tmpdir = $tmpdir;
    }

    //setting access token
    public function set_access_token($token) {

        //setting token
        //TODO safe token regexp
        $this->access_token = $token;

        VKDebug::debug_function(__METHOD__, substr($this->access_token, 0, 10) . "...");

        return $this->access_token;
    }

    public function get_access_token() {

        VKDebug::debug_retval(__METHOD__, substr($this->access_token, 0, 10) . "...");

        return $this->access_token;
    }

    public function get_access_token_short() {
        VKDebug::debug_retval(__METHOD__, substr($this->access_token, 0, 8));

        return substr($this->access_token, 0, 8);
    }
    
    public function date_access_token_short() {
        VKDebug::debug_retval(__METHOD__, date("Y-m-d")." ".substr($this->access_token, 0, 8));

        return date("Y-m-d")." ".substr($this->access_token, 0, 8);
    }

    //API CALL TO VK.COM
    //TODO
    //html encode the whole query?
    private function api($method, $arguments, $need_token = self::SKIP_ACCESS_TOKEN, $sleep_us = NULL) {

        //API URL
        $url = "https://api.vk.com/method/$method?$arguments&v=5.45";
        $query = "$method ($arguments)";
        $key = substr($need_token == self::NEED_ACCESS_TOKEN ? $this->access_token : "NO_TOKEN", 0, 8) . " -> $query";

        //Add access token if needed
        if ($need_token == self::NEED_ACCESS_TOKEN) {
            if (!$this->access_token) {
                throw new \Exception("Access token not set");
            }
            
            $url .= "&access_token=$this->access_token";

            //Sleep between API calls to avoid negative circumstances 
            if ($sleep_us === NULL) {
                usleep($this->sleep_microseconds);
            } else {
                usleep($sleep_us);
            }
        }

        if ($this->api_history) {
            $this->api_history->write($key);
        }

        //HTTP request to API URL
        $result = VKMisc::get_http($url);

        //Parsing and forming a JSON
        $response = json_decode($result);

        if ($this->api_history) {
            $this->api_history->delete($key);
            $this->api_history->write($key, $result);
        }

        VKDebug::debug_api(__METHOD__, $method, $arguments, $result);

        //Throwing exceptions
        if (!$response) {
            throw new \Exception("No response from API");
        }

        if ($response->error) {
            if ($response->error->error_code == 14 && $this->captcha_history) {
                $this->captcha_history->write($this->get_access_token_short(), $query);
            }
            throw new \Exception($response->error->error_msg, $response->error->error_code);
        }

        return $response->response;
    }

    //Setting the KEY / VALUE pair at VK application global storage
    //Suitable for small pieces of information and rare access
    public function global_storage_set($key, $value) {

        VKDebug::debug_function(__METHOD__, $key, $value);

        //API call
        $result = $this->api('storage.set', "key=" . urlencode($key) . "&value=" . urlencode($value) . "&global=1", self::NEED_ACCESS_TOKEN);

        if ($result != 1) {
            //TODO: Set a code
            throw new \Exception("Unable to set a storage value");
        }

        return $value;
    }

    //Get the key's value from VK app global storage
    public function global_storage_get($key) {

        //API call
        $result = $this->api('storage.get', "key=" . urlencode($key) . "&global=1", self::NEED_ACCESS_TOKEN);

        VKDebug::debug_retval(__METHOD__, $key, $result);

        return $result;
    }

    //Broadcast audio to a person or a group
    //TODO?: multiple target IDS
    public function audio_set_broadcast_to_owner(VKAudio $audio, VKOwner $owner) {

        //API call
        $result = $this->api('audio.setBroadcast', "target_ids=" . $owner->id() . "&audio=" . $audio->owner_id() . "_" . $audio->id(), self::NEED_ACCESS_TOKEN);

        VKDebug::debug_retval(__METHOD__, $audio->artist(), $audio->title(), $result);

        return $result;
    }

    //get popular audio tracks at VK
    //max_count is limited by 250
    //genre list is implemented in VKGenre
    public function audio_get_popular($music_genre, $max_count, $english_only_filter) {

        $genre = (int) $music_genre;
        $count = (int) $max_count;
        $english_only = (int) $english_only_filter;

        VKDebug::debug_function(__METHOD__, "VKGenre#$genre", "count#$count", "english?$english_only");

        //API call
        $result = $this->api('audio.getPopular', "only_eng=$english_only&count=1000&genre_id=$genre", self::NEED_ACCESS_TOKEN);

        //fetching results
        $popular = Array();
        foreach ($result as $audio) {

            //Initializing new VKAudio instances
            $audio_instance = new VKAudio($audio);

            if ($audio_instance->meet_basic_requirements()) {
                array_push($popular, $audio_instance);
            }
        }

        return $popular;
    }

    //Get document upload server URL
    public function document_get_upload_server() {

        //API call
        $result = $this->api('docs.getUploadServer', "", self::NEED_ACCESS_TOKEN);

        VKDebug::debug_retval(__METHOD__, $result->upload_url);

        return $result->upload_url;
    }

    //upload document to group wall scenario
    public function document_upload_group_wall_from_url(VKGroup $group, $url, $title = "gif", $tag = "") {

        VKDebug::debug_function(__METHOD__, $group->title(), $url, $title, $tag);

        //get upload server URL
        $upload_url = $this->document_get_upload_server();
        //upload file from other URL to server
        $file = $this->document_upload_from_url_to_url($url, $upload_url);
        //save group document
        $documents = $this->wall_group_save_document($file, $title, $tag);

        return $documents;
    }

    //Get group wall upload server URL for photo uploading
    public function photo_get_group_wall_upload_server(VKGroup $group) {

        //API call
        $result = $this->api('photos.getWallUploadServer', "group_id=" . $group->unsigned_id(), self::NEED_ACCESS_TOKEN);

        VKDebug::debug_retval(__METHOD__, $result->upload_url);

        return $result->upload_url;
    }

    //upload photo to group wall scenario
    public function photo_upload_group_wall_from_url(VKGroup $group, $url) {

        VKDebug::debug_function(__METHOD__, $group->title(), $url);

        //get upload url
        $upload_url = $this->photo_get_group_wall_upload_server($group);
        //upload from one url to another using tempfile
        $result = $this->photo_upload_to_wall_from_url_to_url($url, $upload_url);
        //save photo and get an instance of VKPhoto
        $photo = $this->wall_group_save_photo($result->server, $result->photo, $result->hash, $group);

        return $photo;
    }

    //Download photo from intenet
    //Save it to tempfile
    //Upload tempfile to VK server 
    //Get upload data for further saving

    public function photo_upload_to_wall_from_url_to_url($photo_url, $upload_url) {

        VKDebug::debug_function(__METHOD__, $photo_url, $upload_url);

        //download photo
        $tmp = VKMisc::tempfile_from_url($photo_url, $this->tmpdir);

        //form parameter array for POST 
        $files = array('photo' => '@' . $tmp);

        //post file
        $photo = VKMisc::post_files($files, $upload_url);

        //delete tempfile
        unlink($tmp);

        return $photo;
    }

    //Download document from intenet
    //Save it to tempfile
    //Upload tempfile to VK server 
    //Get upload data for further saving
    public function document_upload_from_url_to_url($document_url, $upload_url) {

        VKDebug::debug_function(__METHOD__, $document_url, $upload_url);

        //download document to tempfile
        $tmp = VKMisc::tempfile_from_url($document_url, $this->tmpdir);

        //prepare parameters for POST
        $files = array('file' => '@' . $tmp);

        //post files
        $response = VKMisc::post_files($files, $upload_url);

        //delete tempfile
        unlink($tmp);

        return $response->file;
    }

    //delete document as an owner
    public function document_delete(VKDocument $document) {

        VKDebug::debug_function(__METHOD__, $document->get_string(), $document->title());

        //API call
        $this->api('docs.delete', "owner_id=" . $document->owner_id() .
                "&doc_id=" . $document->id(), self::NEED_ACCESS_TOKEN);
    }

    //Save photo by commiting upload data back to VK server
    //VKPhoto instance is returned
    public function wall_group_save_photo($server, $photo, $hash, $group) {

        VKDebug::debug_function(__METHOD__, $server, $photo, $hash, $group);

        $result = $this->api("photos.saveWallPhoto", "server=$server&photo=$photo&hash=$hash&group_id=" . $group->unsigned_id(), self::NEED_ACCESS_TOKEN);

        $saved_photo = new VKPhoto($result[0]);

        return $saved_photo;
    }

    //save uploaded file to the group
    //by commiting the upload data
    //new instance of VKDocument is created on success
    public function wall_group_save_document($file, $title = "gifanky", $tag = "gifanky") {

        VKDebug::debug_function(__METHOD__, $file, $title, $tag);

        //API call
        $result = $this->api("docs.save", "file=$file&title=$title&tag=$tag", self::NEED_ACCESS_TOKEN);

        return new VKDocument($result[0]);
    }

    //get posts from the wall of a group or a person
    public function wall_get(VKOwner $owner, $count, $offset, $filter = VKPost::FILTER_ALL, $need_access_token = VK::NEED_ACCESS_TOKEN) {

        VKDebug::debug_function(__METHOD__, "VKOwner#" . $owner->id(), "count#$count", "offset#$offset", "filter#$filter", "need_token?$need_access_token");

        if ($need_access_token == VK::SKIP_ACCESS_TOKEN) {
            $need_access_token = VK::NEED_ACCESS_TOKEN;
            VKDebug::debug("wall_get does not work without access token. Forcing current token.");
        }
        //array for posts
        $posts = Array();

        try {
            //API call
            $result = $this->api('wall.get', "owner_id=" . $owner->id() .
                    "&offset=" . (int) $offset .
                    "&count=" . (int) $count .
                    "&filter=" . urlencode($filter), $need_access_token
            );
        } catch (\Exception $e) {

            VKDebug::debug_retval(__METHOD__, $e->getMessage());

            //Return empty array if the page of a user was deleted
            //Or access was denied
            //Otherwise rethrow the exception

            switch ($e->getCode()) {
                case 15:
                case 18: return $posts;
                default: throw $e;
            }
        }

        //Fetching the results and initializing new VKPosts
        foreach ($result->items as $post) {
            array_push($posts, new VKPost($post));
        }
        return $posts;
    }

    //Post on a wall of a group
    public function wall_post(VKOwner $owner, $message, $attachments = NULL, $from_group = true) {

        VKDebug::debug_function(__METHOD__, "VKOwner#" . $owner->id(), $message, "attachments#" . sizeof($attachments));

        //form a string of attachments
        //maximum 10 attachments
        if ($attachments) {
            $attachments_string = "";
            foreach ($attachments as $attachment) {
                $attachments_string .= $attachment->get_string() . ",";
            }
        }

        //encode attachment string
        //$attachments_string .= urlencode($link);
        //API call
        try {
            $result = $this->api('wall.post', "owner_id=" . $owner->id() .
                    "&message=" . urlencode($message) .
                    "&attachments=$attachments_string" .
                    ($from_group == true ? "&from_group=1" : ""), self::NEED_ACCESS_TOKEN);
            
            //Increase the post counter for a group
            $this->post_history->increment($owner->date_id());
            
            //Increase the post counter for token posts
            $this->post_history->increment($this->date_access_token_short());
            
        } catch (\Exception $e){
            throw $e;
        }

        $post->owner_id = $owner->id();
        $post->id = (int) $result->post_id;

        return new VKPost($post);
    }

    //Post on a wall of a group
    public function wall_delete(VKPost $post) {

        VKDebug::debug_function(__METHOD__, $post->get_string());

        //API call
        $this->api('wall.delete', "owner_id=" . $post->owner_id() .
                "&post_id=" . $post->id(), self::NEED_ACCESS_TOKEN);
    }

    //return temporary dir route
    public function tmpdir() {
        return $this->tmpdir;
    }

    //repost a post to a group wall
    public function group_wall_repost(VKAttachment $attachment, VKGroup $group, $message = "") {

        VKDebug::debug_function(__METHOD__, "VKGroup#" . $group->title(), "$message", $attachment->get_string());

        try {
        //API call
            $result = $this->api('wall.repost', "object=" . $attachment->get_string() .
                "&message=" . urlencode($message) .
                "&group_id=" . $group->unsigned_id(), self::NEED_ACCESS_TOKEN);
        
            //Increase the post counter for a group
            $this->post_history->increment($group->date_id());
            
            //Increase the post counter for token posts
            $this->post_history->increment($this->date_access_token_short());

        } catch (\Exception $e){
            throw $e;
        }

    }

    //get updated instance of VKDocument
    public function document_get_updated(VKDocument $document) {
        VKDebug::debug_function(__METHOD__, $document->get_string());

        $result = NULL;

        //API call
        $response = $this->api('docs.getById', "docs=" . $document->get_string_without_prefix(), self::NEED_ACCESS_TOKEN);

        foreach ($response as $doc) {
            $result = new VKDocument($doc);
            break;
        }

        return $result;
    }

    //check whether document still exists
    public function document_exists(VKDocument $document) {
        VKDebug::debug_function(__METHOD__, $document->get_string());

        //get the updated version of a document
        $updated_document = $this->document_get_updated($document);

        return $updated_document != NULL;
    }

    //get the latest group post by the posting time
    public function group_last_post(VKGroup $group, $need_access_token = VK::NEED_ACCESS_TOKEN) {

        VKDebug::debug_function(__METHOD__, "VKGroup#" . $group->title());

        //API call
        //get last 2 posts
        $posts = $this->wall_get($group, 2, 0, VKPost::FILTER_OWNER, $need_access_token);

        $maximum_date = 0;
        $latest_post = NULL;

        //compare datetimes of these posts
        //in case of pinned post being the first one
        foreach ($posts as $post) {
            if ($post->date() > $maximum_date) {
                $maximum_date = $post->date();
                $latest_post = $post;
            }
        }

        return $latest_post;
    }

    public function attachment_like_add(VKAttachment $attachment) {
        VKDebug::debug_function(__METHOD__, $attachment->get_string());

        $this->api('likes.add', "type=" . $attachment->type_like() .
                "&owner_id=" . $attachment->owner_id() .
                "&item_id=" . $attachment->id(), self::NEED_ACCESS_TOKEN);
    }

    public function attachment_like_delete(VKAttachment $attachment) {
        VKDebug::debug_function(__METHOD__, $attachment->get_string());

        $this->api('likes.delete', "type=" . $attachment->type_like() .
                "&owner_id=" . $attachment->owner_id() .
                "&item_id=" . $attachment->id(), self::NEED_ACCESS_TOKEN);
    }

    public function user_get_updated(VKUser $user, $need_token = self::SKIP_ACCESS_TOKEN) {
        VKDebug::debug_function(__METHOD__, "VKUser#" . $user->id(), "need_token?$need_token");

        $result = $this->users_get_updated(array($user), $need_token);

        return $result['id' . $user->id()];
    }

    public function users_get_updated($users, $need_token = self::SKIP_ACCESS_TOKEN) {
        VKDebug::debug_function(__METHOD__, "VKUser[" . sizeof($users) . "]", "need_token?$need_token");

        //list all the users to update
        $user_hashtable = array();
        $user_string_list = "";
        foreach ($users as $user) {
            $user_string_list .= $user->id() . ",";
            $user_hashtable["id" . $user->id()] = $user;
        }

        //API call
        $response = $this->api('users.get', "user_ids=$user_string_list&fields=sex,counters,can_write_private_message,can_post,has_photo,wall_comments,movies,music,quotes,exports", $need_token);

        foreach ($response as $updated_user_data) {
            //$updated_user = new VKUser($updated_user_data->id);
            //$updated_user->fetch($updated_user_data);
            //$user_hashtable["id".$updated_user->id()] = $updated_user;
            $user_hashtable["id" . $updated_user_data->id]->fetch($updated_user_data);
        }

        return $user_hashtable;
    }

    public function users_search($need_access_token = VK::SKIP_ACCESS_TOKEN, $query = self::PARAM_QUERY, $sort = self::SORT_POPULAR, $offset = 0, $count = self::MAX_USERS_PER_QUERY, $sex = VKUser::GENDER_UNSPECIFIED, $group_id = NULL, $has_photo = VKUser::HAS_PHOTO, $online = VKUser::ONLINE, $age_from = 0, $age_to = 100, $status = NULL) {
        VKDebug::debug_function(__METHOD__, "query: $query", "offset: $offset", "count: $count", "sex: $sex");

        $users = Array();
        //API call
        $response = $this->api('users.search', "q=$query&"
                . "sort=$sort"
                . "&offset=$offset"
                . "&count=$count"
                . "&sex=$sex"
                . "&group_id=$group_id"
                . "&has_photo=$has_photo"
                . "&fields=sex,counters"
                . "&online=$online"
                . "&age_from=$age_from"
                . "&age_to=$age_to"
                . "&status=$status", $need_access_token);

        foreach ($response->items as $user_data) {
            $user = new VKUser($user_data->id);
            $user->fetch($user_data);
            array_push($users, $user);
        }

        return $users;
    }

    public function friends_search(VKUser $user, $need_access_token = VK::NEED_ACCESS_TOKEN, $query = self::PARAM_QUERY, $sort = self::SORT_POPULAR, $offset = 0, $count = self::MAX_USERS_PER_QUERY, $sex = VKUser::GENDER_UNSPECIFIED, $group_id = NULL, $has_photo = VKUser::HAS_PHOTO, $online = VKUser::ONLINE, $age_from = 0, $age_to = 100, $status = NULL) {
        VKDebug::debug_function(__METHOD__, "user: " . $user->id(), "query: $query", "offset: $offset", "count: $count", "sex: $sex");

        $users = Array();
        //API call
        $response = $this->api('friends.search', "user_id=" . $user->id()
                . ($query ? "&q=$query" : "")
                . "&sort=$sort"
                . "&offset=$offset"
                . "&count=$count"
                . "&sex=$sex"
                . "&group_id=$group_id"
                . "&has_photo=$has_photo"
                . "&fields=sex,counters"
                . "&online=$online"
                . "&age_from=$age_from"
                . "&age_to=$age_to"
                . "&status=$status", $need_access_token);

        foreach ($response->items as $user_data) {
            $user = new VKUser($user_data->id);
            $user->fetch($user_data);
            array_push($users, $user);
        }

        return $users;
    }

    public function groups_get_members(VKGroup $group, $need_access_token = VK::NEED_ACCESS_TOKEN, $sort = self::SORT_MEMBER_ID_ASC, $count = self::MAX_USERS_PER_QUERY, $offset = 0) {
        VKDebug::debug_function(__METHOD__, "group: " . $group->id(), "offset: $offset", "count: $count", "sort: $sort");

        $users = Array();
        //API call
        $response = $this->api('groups.getMembers', "group_id=" . $group->unsigned_id()
                . "&sort=$sort"
                . "&offset=$offset"
                . "&count=$count&fields=sex"
                , $need_access_token);

        foreach ($response->items as $user_data) {
            $user = new VKUser($user_data->id);
            $user->fetch($user_data);
            array_push($users, $user);
        }

        return $users;
    }

    public function account_get_profile_info() {
        VKDebug::debug_function(__METHOD__);

        $response = $this->api("account.getProfileInfo", '', VK::NEED_ACCESS_TOKEN);
        return $response;
    }

    public function account_set_online() {
        VKDebug::debug_function(__METHOD__);

        $this->api("account.setOnline", 'voip=0', VK::NEED_ACCESS_TOKEN);
    }

    public function messages_set_activity(VKOwner $owner) {
        $this->api("messages.setActivity", 'type=typing&peer_id=' . $owner->id(), VK::NEED_ACCESS_TOKEN);
    }

    //account.getProfileInfo
}

/*
function song_analog($song, $access_token, $sleep_seconds){
	
	
}*/





