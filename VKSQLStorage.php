<?php

namespace SocialAutomation\VK;

class VKSQLStorage implements Interfaces\VKStorageInterface {

    const FORWARD = 0;
    const REVERSE = 1;
    
    private $history;
    private $name;
    private $type;

    public function __construct($connection, $id, $name, $type = self::FORWARD) {

        //TODO: somehow escape $id, $name?
        $this->history = new VKSQLHistory($connection, VKHistory::VARIABLES, VKSQLHistory::MAX_HISTORY_LENGTH, $id);
        $this->name = $name;
        $this->type = $type;

        VKDebug::debug_construct($this, "id#$id", "name#$name");
    }

    //getter
    public function get() {
        //get the value
        $value = $this->history->value($this->name);
        if ($this->type == self::REVERSE){
            $value = strrev($value);
        }
        VKDebug::debug_retval(__METHOD__, mb_substr($value, 0, 15)."...");
        return $value;
    }

    //setter
    public function set($value) {
        VKDebug::debug_function(__METHOD__, $value);
        if ($this->type == self::REVERSE){
            $value = strrev($value);
        }
        $this->history->write($this->name, $value);
    }

}
